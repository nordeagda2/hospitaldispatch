package com.amen.hospital.event.events;

/**
 * Created by amen on 9/15/17.
 */
public interface IEvent {
    void run();
}
