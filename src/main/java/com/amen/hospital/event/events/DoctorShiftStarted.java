package com.amen.hospital.event.events;

import com.amen.hospital.event.EventDispatcher;
import com.amen.hospital.event.listeners.IDoctorShiftListener;
import com.amen.hospital.event.listeners.IRoomPatientListener;
import com.amen.hospital.units.Doctor;
import com.amen.hospital.units.PatientState;

import java.util.List;

public class DoctorShiftStarted implements IEvent {
    private Doctor doc;

    public DoctorShiftStarted(Doctor doc) {
        this.doc = doc;
    }

    @Override
    public void run() {
        List<IDoctorShiftListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IDoctorShiftListener.class);
        for (IDoctorShiftListener listener : list) {
            listener.doctorShiftStarted(doc);
        }
    }
}
