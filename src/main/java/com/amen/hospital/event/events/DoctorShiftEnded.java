package com.amen.hospital.event.events;

import com.amen.hospital.event.EventDispatcher;
import com.amen.hospital.event.listeners.IDoctorShiftListener;
import com.amen.hospital.units.Doctor;

import java.util.List;

public class DoctorShiftEnded implements IEvent {
    private Doctor doc;

    public DoctorShiftEnded(Doctor doc) {
        this.doc = doc;
    }

    @Override
    public void run() {
        List<IDoctorShiftListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IDoctorShiftListener.class);
        for (IDoctorShiftListener listener : list) {
            listener.doctorShiftEnded(doc);
        }
    }
}
