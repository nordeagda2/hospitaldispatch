package com.amen.hospital.event.events;

import com.amen.hospital.event.EventDispatcher;
import com.amen.hospital.event.listeners.IRoomPatientListener;
import com.amen.hospital.units.PatientState;
import com.amen.hospital.units.Room;

import java.util.List;

public class PatientAlertEvent implements IEvent {
    private int room;

    public PatientAlertEvent(int room, PatientState state) {
        this.room = room;
    }

    @Override
    public void run() {
        boolean maybe_it_should_maybe_it_souldnt = true; // understand this !!!!
        // powiadamiamy wszystkich o zmianie stanu pacjenta
        List<IRoomPatientListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IRoomPatientListener.class);
        for (IRoomPatientListener listener : list) {
            for (Room coveringRoom : listener.getCoveringRooms()) {
                if (coveringRoom.getRoom_id() == room) {
                    listener.patientStateAlert(room);
                }
            }
        }
    }
}