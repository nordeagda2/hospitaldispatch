package com.amen.hospital.event.events;

import com.amen.hospital.event.EventDispatcher;
import com.amen.hospital.event.listeners.IRoomPatientListener;
import com.amen.hospital.units.PatientState;
import com.amen.hospital.units.Room;

import java.util.List;

public class PatientStateChangedEvent implements IEvent {
    private int room;
    private PatientState state;

    public PatientStateChangedEvent(int room, PatientState state) {
        this.room = room;
        this.state = state;
    }

    @Override
    public void run() {
        List<IRoomPatientListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IRoomPatientListener.class);
        for (IRoomPatientListener listener : list) {
            for (Room coveringRoom : listener.getCoveringRooms()) {
                if (coveringRoom.getRoom_id() == room) {
                    listener.patientStateAlert(room);
                }
            }
        }
    }
}
