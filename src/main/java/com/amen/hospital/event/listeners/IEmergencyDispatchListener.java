package com.amen.hospital.event.listeners;

/**
 * Created by amen on 9/15/17.
 */
public interface IEmergencyDispatchListener {
    void emergencyCalled(String address);
}
