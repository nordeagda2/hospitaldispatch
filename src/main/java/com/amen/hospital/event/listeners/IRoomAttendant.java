package com.amen.hospital.event.listeners;

import com.amen.hospital.units.Room;

import java.util.List;

/**
 * Created by amen on 9/15/17.
 */
public interface IRoomAttendant {
    List<Room> getCoveringRooms();
    boolean isFree();
}
