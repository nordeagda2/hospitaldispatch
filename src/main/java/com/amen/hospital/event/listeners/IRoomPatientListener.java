package com.amen.hospital.event.listeners;

import com.amen.hospital.units.PatientState;

/**
 * Created by amen on 9/15/17.
 */
public interface IRoomPatientListener extends IRoomAttendant {
    void patientStateAlert(int room);
    void patientStateChanged(int room, PatientState state);
}
