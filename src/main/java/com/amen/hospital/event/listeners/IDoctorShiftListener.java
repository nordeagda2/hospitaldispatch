package com.amen.hospital.event.listeners;

import com.amen.hospital.units.Doctor;

public interface IDoctorShiftListener {
    void doctorShiftStarted(Doctor d);
    void doctorShiftEnded(Doctor d);
}
