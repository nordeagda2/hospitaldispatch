package com.amen.hospital;

import com.amen.hospital.event.EventDispatcher;
import com.amen.hospital.event.listeners.IDoctorShiftListener;
import com.amen.hospital.units.Doctor;
import com.amen.hospital.units.PatientState;
import com.amen.hospital.units.Room;

import javax.print.Doc;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by amen on 9/15/17.
 */
public class Hospital implements IDoctorShiftListener {

    public Hospital() {
        EventDispatcher.instance.registerObject(this);
        int room_id;

        Room room = new Room(2, 90);
        new Doctor(true,
                Arrays.asList(
                        new Room(0, 90),
                        new Room(1, 90),
                        room),
                "Marian", 2);

        new Doctor(true,
                Arrays.asList(
                        room,
                        new Room(3, 90),
                        new Room(4, 90)),
                "Michau", 2);
    }

    public static void main(String[] args) {
        new Hospital();
    }

    @Override
    public void doctorShiftStarted(Doctor d) {
        System.out.println("Lekarz zaczął pracę : " + d.getName());
    }

    @Override
    public void doctorShiftEnded(Doctor d) {
        System.out.println("Lekarz skończył pracę : " + d.getName());
    }
}
