package com.amen.hospital.units;

import com.amen.hospital.event.EventDispatcher;

/**
 * Created by amen on 9/15/17.
 */
public abstract class AbstractUnit {
    // zwróć uwagę, ta klasa już odpowiada za zarejestrowanie obiektu w Event Dispatcherze
    public AbstractUnit() {
        EventDispatcher.instance.registerObject(this);
    }
}
