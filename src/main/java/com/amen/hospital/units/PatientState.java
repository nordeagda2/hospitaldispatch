package com.amen.hospital.units;

public enum PatientState {
    GOOD, BAD, CRITICAL, STABLE;
}
