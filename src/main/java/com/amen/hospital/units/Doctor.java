package com.amen.hospital.units;

import com.amen.hospital.event.listeners.IRoomPatientListener;

import java.util.*;
import java.util.concurrent.Executors;

/**
 * Created by amen on 9/15/17.
 */
public class Doctor extends AbstractUnit implements IRoomPatientListener, Runnable {
    private boolean isFree;
    private List<Room> roomsCovered;
    private List<Room> priorityRooms;
    private String name;

    private int shiftLength;

    private PriorityQueue<Room> priorityQueue;

    public Doctor(boolean isOnDuty, List<Room> roomsCovered, String name, int shiftLength) {
        super();
        this.isFree = isOnDuty;
        this.name = name;
        this.shiftLength = shiftLength;
        this.roomsCovered = roomsCovered;

        this.priorityRooms = new ArrayList<>();

        this.priorityQueue = new PriorityQueue<>(new Comparator<Room>() {
            @Override
            public int compare(Room o1, Room o2) {
                return o1.getPunkty_zycia() > o2.getPunkty_zycia() ? 1 : -1;
            }
        });

        Executors.newSingleThreadExecutor().submit(this);
    }

    @Override
    public void patientStateAlert(int room) {

    }

    @Override
    public List<Room> getCoveringRooms() {
        return roomsCovered;
    }

    @Override
    public void patientStateChanged(int room, PatientState state) {
        System.out.println("otrzymałem status pacjenta z pokoju: " + room + " -> " + state);
        for (Room pokoj : roomsCovered) {
            if (pokoj.getRoom_id() == room &&
                    (state == PatientState.CRITICAL || state == PatientState.BAD)) {
                priorityRooms.add(pokoj);
                return;
            }
        }
    }

    @Override
    public boolean isFree() {
        return isFree;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(name);
        while (shiftLength-- > 0) {
            Iterator<Room> it = priorityRooms.iterator();
            while (it.hasNext()) {
                Room r = it.next();
                handleRoom(r);
                if (r.getPunkty_zycia() > 30) {
                    it.remove();
                }
            }
            for (Room r : roomsCovered) {
                handleRoom(r);
                if (!priorityRooms.isEmpty()) {
                    break;
                }
            }
        }
    }

    private void handleRoom(Room r) {
        r.setPunkty_zycia(r.getPunkty_zycia() + 10);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }
}
