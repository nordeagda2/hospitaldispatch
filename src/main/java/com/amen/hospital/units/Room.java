package com.amen.hospital.units;

import com.amen.hospital.event.EventDispatcher;
import com.amen.hospital.event.events.PatientStateChangedEvent;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by amen on 9/15/17.
 */
public class Room extends AbstractUnit {

    private int room_id;

    private PatientState state;
    private int punkty_zycia;

    private Timer timer;

    public Room(int room_id, int punktyzycia) {
        super();
        this.room_id = room_id;
        this.punkty_zycia = punktyzycia;

        // uruchom zadanie z opoznieniem 10 sek i uruchamiaj je co 8 sek
        this.timer = new Timer(String.valueOf(room_id));
        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Random r = new Random();

                punkty_zycia -= r.nextInt(20);

                if (punkty_zycia < 10) {
                    state = PatientState.CRITICAL;
                } else if (punkty_zycia < 30) {
                    state = PatientState.BAD;
                } else if (punkty_zycia < 80) {
                    state = PatientState.STABLE;
                } else {
                    state = PatientState.GOOD;
                }

                EventDispatcher.instance.dispatch(new PatientStateChangedEvent(room_id, state));

            }
        }, 10000, 8000);
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public PatientState getState() {
        return state;
    }

    public void setState(PatientState state) {
        this.state = state;
    }

    public int getPunkty_zycia() {
        return punkty_zycia;
    }

    public void setPunkty_zycia(int punkty_zycia) {
        this.punkty_zycia = punkty_zycia;
    }
}
